#!/bin/bash

TMPDIR=$(mktemp -d)
BUILDDIR=$(pwd)
ARCHDIR=x64
ARCH=x86_64
DISTDIR=distro

pacman -Syu
pacman -S chroot wget

cd $TMPDIR

wget "https://raw.githubusercontent.com/tokland/arch-bootstrap/master/arch-bootstrap.sh"
wget "https://raw.githubusercontent.com/tokland/arch-bootstrap/master/get-pacman-dependencies.sh"

install -m 755 arch-bootstrap.sh /usr/local/bin/arch-bootstrap

mkdir $DISTDIR
arch-bootstrap -a $ARCH $DISTDIR

mount --bind /dev $TMPDIR/$DISTDIR/dev

chroot $DISTDIR pacman -Syyu --noconfirm
chroot $DISTDIR pacman -S systemd-sysvcompat --noconfirm
chroot $DISTDIR pacman -S sudo git --noconfirm
chroot $DISTDIR pacman -Rns $(pacman -Qtdq)

chroot $DISTDIR rm /etc/resolv.conf
cp $BUILDDIR/linux_files/sudoers $DISTDIR/etc/sudoers

umount $TMPDIR/$DISTDIR/dev

mkdir -p $BUILDDIR/$ARCHDIR

cd $DISTDIR
tar --numeric-owner -czf "$BUILDDIR/$ARCHDIR/install.tar.gz" *

cd $BUILDDIR
rm -rf $TMPDIR